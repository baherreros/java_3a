package app.repository;

import app.entity.Nutrition_score;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;


import java.util.List;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class Nutrition_ScoreRepositoryTest
{
    @Autowired
    private TestEntityManager entityManager;


    @Autowired
    private Nutrition_ScoreRepository repository;

    @Test
    public void testFindByScoreBornes() {


        //Check size
        List<Nutrition_score> l = repository.findAll();
        assertEquals(l.size(),5);


        //Check Bornes
        Nutrition_score ns = repository.findByScore(-10);
        assertEquals(ns.getId() , 1L);

        ns = repository.findByScore(-1);
        assertEquals(ns.getId() , 1L);

        ns = repository.findByScore(0);
        assertEquals(ns.getId() , 2L);

        ns = repository.findByScore(2);
        assertEquals(ns.getId() , 2L);

        ns = repository.findByScore(3);
        assertEquals(ns.getId() , 3L);

        ns = repository.findByScore(10);
        assertEquals(ns.getId() , 3L);

        ns = repository.findByScore(11);
        assertEquals(ns.getId() , 4L);

        ns = repository.findByScore(18);
        assertEquals(ns.getId() , 4L);

        ns = repository.findByScore(19);
        assertEquals(ns.getId() , 5L);

        ns = repository.findByScore(40);
        assertEquals(ns.getId() , 5L);

    }

    @Test
    public void testFindByScoreMiddle() {


        Nutrition_score ns = repository.findByScore(-5);
        assertEquals(ns.getId() , 1L);

        ns = repository.findByScore(1);
        assertEquals(ns.getId() , 2L);

        ns = repository.findByScore(5);
        assertEquals(ns.getId() , 3L);

        ns = repository.findByScore(15);
        assertEquals(ns.getId() , 4L);

        ns = repository.findByScore(25);
        assertEquals(ns.getId() , 5L);

    }

    @Test
    public void testFindByScoreOut() {

        List<Nutrition_score> l = repository.findAll();
        assertEquals(l.size(),5);

        Nutrition_score ns = repository.findByScore(-20);
        assertEquals(ns , null);

        ns = repository.findByScore(50);
        assertEquals(ns , null);


    }



}
