package app.repository;

import app.entity.Product;
import app.entity.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class ProductRepositoryTest
{
    @Autowired
    private TestEntityManager entityManager;


    @Autowired
    private ProductRepository repository;

    @Test
    public void TestFindByBarcode() {

        entityManager.persist(new Product("7622210449283", "Test1", 9, "Mangeable", "yellow"));
        entityManager.persist(new Product("7622210449284", "Test2", 12, "Mouai", "orange"));

        //Check size
        List<Product> l = repository.findAll();
        assertEquals(l.size(), 2);

        //Check existing product
        String barcode = "7622210449283";
        Product p = repository.findByBarcode(barcode);
        assertNotNull(p);
        assertEquals(p.getBarcode(), barcode);

        //Check non existing barcode
        p = repository.findByBarcode("cheval");
        assertNull(p);

    }


}
