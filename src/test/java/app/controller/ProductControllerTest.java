package app.controller;

import app.entity.Product;
import app.service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ProductController.class)
public class    ProductControllerTest
{
    @MockBean
    private ProductService productService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetExistingBarcode() throws Exception {
        String barcode = "7622210449283";
        Product product = new Product(1L, barcode, "test1", 9, "Mangeable", "yellow");

        Mockito.when(productService.getByBarcode(barcode)).thenReturn(product);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/"+barcode))
                .andExpect(content().string("{\"id\":1,\"barcode\":\"7622210449283\",\"name\":\"test1\",\"nutritionScore\":9,\"classe\":\"Mangeable\",\"color\":\"yellow\"}"));
    }

    @Test
    public void testGetNonExistingBarcode() throws Exception {
        String barcode = "cheval";
        Product product = new Product(1L, barcode, "test1", 9, "Mangeable", "yellow");

        Mockito.when(productService.getByBarcode(barcode)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/"+barcode))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }




}
