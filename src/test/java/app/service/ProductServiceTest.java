package app.service;

import app.entity.Nutrition_score;
import app.entity.OFFProduct;
import app.entity.Product;
import app.repository.Nutrition_ScoreRepository;
import app.repository.ProductRepository;
import app.repository.RuleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@Import(ProductService.class)
public class ProductServiceTest
{
    @MockBean
    private OFFProductService offProductService;
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private Nutrition_ScoreRepository nutrition_scoreRepository;
    @MockBean
    private RuleRepository ruleRepository;

    @Autowired
    private ProductService service;

    @Test
    void TestGetByBarcodeExistingInBDD() {
        String[] N = {"energy_100g", "saturated-fat_100g", "sugars_100g", "salt_100g"};
        String[] P = {"fiber_100g", "proteins_100g"};

        double[] N_values = {1955, 5.6, 32, 0.58};
        double[] P_values = {4, 6.4};

        int[] N_res = {5, 5, 7, 0};
        int[] P_res = {4, 4};

        String barcode = "7622210449283";
        Product product = new Product(barcode, "Test1", 9, "Mangeable", "yellow");

        //Mock rule
        for (int i = 0; i < N.length; ++i)
        {
            Mockito.when(ruleRepository.findByNameAndValue(N[i], N_values[i])).thenReturn(N_res[i]);
        }

        for (int i = 0; i < P.length; ++i)
        {
            Mockito.when(ruleRepository.findByNameAndValue(P[i], P_values[i])).thenReturn(P_res[i]);
        }

        //Mock nutrition_score
        Mockito.when(nutrition_scoreRepository.findByScore(product.getNutritionScore())).thenReturn(new Nutrition_score(3L, "Mangeable", 3, 10, "yellow"));

        //Mock product
        Mockito.when(productRepository.findByBarcode(barcode)).thenReturn(product);

        //Test existing barcode
        Product newProduct = service.getByBarcode(barcode);

        assertEquals(product, newProduct);

    }

    @Test
    void TestGetByBarcodeExistingNotInBDD() {
        String[] N = {"energy_100g", "saturated-fat_100g", "sugars_100g", "salt_100g"};
        String[] P = {"fiber_100g", "proteins_100g"};

        double[] N_values = {1955, 5.6, 32, 0.58};
        double[] P_values = {4, 6.4};

        int[] N_res = {5, 5, 7, 0};
        int[] P_res = {4, 4};

        String json = "{\n" +
                "  \"code\": \"7622210449283\",\n" +
                "  \"product\": {\n" +
                "    \"generic_name\": \"Test1\",\n" +
                "    \"nutriments\":{\n" +
                "     \"sugars_100g\":32,\n" +
                "     \"fiber_100g\":4,\n" +
                "     \"proteins_100g\":6.4,\n" +
                "     \"energy_100g\":1955,\n" +
                "     \"saturated-fat_100g\":5.6,\n" +
                "     \"salt_100g\":0.58\n" +
                "    }\n" +
                "  }\n" +
                "}";

        String barcode = "7622210449283";
        Product product = new Product(barcode, "Test1", 9, "Mangeable", "yellow");
        OFFProduct offProduct = new OFFProduct(barcode, "Test1");

        //Mock rule
        for (int i = 0; i < N.length; ++i)
        {
            Mockito.when(ruleRepository.findByNameAndValue(N[i], N_values[i])).thenReturn(N_res[i]);
            offProduct.addNutriment(N[i], N_values[i]);
        }

        for (int i = 0; i < P.length; ++i)
        {
            Mockito.when(ruleRepository.findByNameAndValue(P[i], P_values[i])).thenReturn(P_res[i]);
            offProduct.addNutriment(P[i], P_values[i]);
        }

        //Mock nutrition_score
        Mockito.when(nutrition_scoreRepository.findByScore(product.getNutritionScore())).thenReturn(new Nutrition_score(3L, "Mangeable", 3, 10, "yellow"));

        //Mock product
        Mockito.when(productRepository.findByBarcode(barcode)).thenReturn(null);
        Mockito.when(productRepository.save(product)).thenReturn(product);


        //Mock OFF Product repository
        Mockito.when(offProductService.getByBarcode(barcode)).thenReturn(offProduct);

        Product newProduct = service.getByBarcode(barcode);

        assertEquals(product, newProduct);

    }

    @Test
    void TestGetByBarcodeNonExisting() {

        String barcode = "cheval";

        //Mock API
        Mockito.when(offProductService.getByBarcode(barcode)).thenReturn(null);

        //Mock product
        Mockito.when(productRepository.findByBarcode(barcode)).thenReturn(null);

        //Test non existing barcode
        Product newProduct = service.getByBarcode(barcode);

        assertNull(newProduct);

    }

    @Test
    void TestCalculNScore() {
        String[] N = {"energy_100g", "saturated-fat_100g", "sugars_100g", "salt_100g"};

        double[] N_values = {1955, 5.6, 32, 0.58};

        int[] N_res = {5, 5, 7, 0};

        OFFProduct offProduct = new OFFProduct("", "");

        //Mock rule
        for (int i = 0; i < N.length; ++i)
        {
            Mockito.when(ruleRepository.findByNameAndValue(N[i], N_values[i])).thenReturn(N_res[i]);
            offProduct.addNutriment(N[i], N_values[i]);
        }


        int res = 0;

        res = service.getNScore(offProduct);

        assertEquals(res, 17);

    }

    @Test
    void TestCalculPScore()
    {
        String[] P = {"fiber_100g", "proteins_100g"};

        double[] P_values = {4, 6.4};
        int[] P_res = {4, 4};

        OFFProduct offProduct = new OFFProduct("", "");

        for (int i = 0; i < P.length; ++i)
        {
            Mockito.when(ruleRepository.findByNameAndValue(P[i], P_values[i])).thenReturn(P_res[i]);
            offProduct.addNutriment(P[i], P_values[i]);
        }

        int res = 0;

        res = service.getPScore(offProduct);

        assertEquals(res, 8);
    }


}
