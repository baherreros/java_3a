package app.service;

import app.entity.OFFProduct;
import app.service.OFFProductService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class OFFProductServiceTest
{
    //@Autowired
    private OFFProductService service = new OFFProductService();

    @Test
    void TestGetByBarcodeFromAPI() throws JSONException {
        //Check existing product
        String barcode = "7622210449283";
        String p = service.getByBarcodeFromAPI(barcode);
        try
        {
            JSONObject obj = new JSONObject(p);
            assertEquals(obj.getString("code"), barcode);

        }catch (Exception e){}

        //Check non existing barcode
        p = service.getByBarcodeFromAPI("cheval");

        JSONObject obj = new JSONObject(p);
        assertEquals(obj.getString("code"), "");


    }

    @Test
    void TestGetByBarcode()
    {

        String[] nutriments = {"energy_100g", "saturated-fat_100g", "sugars_100g", "salt_100g", "fiber_100g", "proteins_100g"};
        double[] nutriments_value = {1955, 5.6, 32, 0.58, 4, 6.4};

        String barcode = "7622210449283";

        //Construction de l'entity
        OFFProduct offProduct = new OFFProduct(barcode, "BISCUITS FOURRÉS (35%) PARFUM CHOCOLAT");

        for (int i = 0; i < nutriments.length; ++i)
        {
            offProduct.addNutriment(nutriments[i], nutriments_value[i]);
        }

        //Existing barcode

        OFFProduct offProductTest = service.getByBarcode(barcode);

        assertEquals(offProduct, offProductTest);

        //Non-existing barcode

        offProductTest = service.getByBarcode("cheval");

        assertNull(offProductTest);

    }

    @Test
    void TestSetNutriments()
    {

        String barcode = "5449000267412";

        //Construction de l'entity
        OFFProduct offProduct = new OFFProduct(barcode, "Boisson rafraîchissante aux extraits végétaux");

        try {
            service.setNutriments(new JSONObject(service.getByBarcodeFromAPI(barcode)), offProduct);
        } catch (JSONException e) { }

        assertEquals(offProduct.getNutriments().size(), 5);

    }


}
