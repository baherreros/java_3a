package app.service;


import app.entity.OFFProduct;
import app.entity.Product;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Fait le lien entre l'api d'Open Food Facts et notre application
 */
@Service
public class OFFProductService
{
    String baseUrl = "https://fr.openfoodfacts.org/api/v0/product/";

    /**
     * Récupére le produit depuis l'API
     * @param barcode : le code barre du produit
     * @return String : le JSON récupéré ou null si le produit n'éxiste pas
     */
    protected String getByBarcodeFromAPI(String barcode)
    {
        try
        {
            //Get le produit sur l'API d'Open Food Facts
            URL url = new URL(baseUrl + barcode);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            //Récupération du code de retour
            int responseCode = con.getResponseCode();

            //Si requête réussie
            if(responseCode == 200)
            {
                //Lire la réponse
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null)
                {
                    response.append(inputLine);
                }

                in.close();

                return response.toString();
            }
            else
            {
                return null;
            }

        }catch (Exception e)
        {
            return null;
        }

    }

    /**
     * Permet de mapper le JSON sur l'entity
     * @param barcode : le code barre du produit
     * @return OFFPRoduct : l'entity contenant les informations de l'API ou null si le produit n'existe pas
     */
    public OFFProduct getByBarcode(String barcode)
    {

        //Récupération du JSON
        String json = getByBarcodeFromAPI(barcode);

        //Si le produit existe
        if(json != null)
        {
            try
            {
                //Parser le json

                JSONObject obj = new JSONObject(json);

                //Création de l'entity
                OFFProduct offProduct = new OFFProduct(barcode, obj.getJSONObject("product").getString("generic_name"));

                //Ajouter les valeurs des nutriments
                setNutriments(obj, offProduct);

                return offProduct;

            } catch (JSONException e)
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    /**
     * Permet de récupérer les valeurs des nutriments dans le JSON
     * @param obj : L'objet JSON
     * @param offProduct : Le produit à remplir
     */
    public void setNutriments(JSONObject obj, OFFProduct offProduct)
    {
        String[] nutriments = {"energy_100g", "saturated-fat_100g", "sugars_100g", "salt_100g", "fiber_100g", "proteins_100g"};

        for (String s: nutriments)
        {
            try
            {
                offProduct.addNutriment(s, obj.getJSONObject("product").getJSONObject("nutriments").getDouble(s));
            } catch (JSONException e) {}

        }
    }

}
