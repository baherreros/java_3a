package app.service;

import app.entity.Nutrition_score;
import app.entity.OFFProduct;
import app.repository.Nutrition_ScoreRepository;
import app.entity.Product;
import app.repository.ProductRepository;
import app.repository.RuleRepository;
import org.json.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Business layer
 * Permet de convertir le json en entity
 * Calculer le score
 * Récupérer la classe et la couleur
 */
@Service
public class ProductService {

    @Autowired
    private OFFProductService offProductService;

    @Autowired
    private Nutrition_ScoreRepository nutrition_scoreRepository;

    @Autowired
    private RuleRepository ruleRepository;

    @Autowired
    private ProductRepository productRepository;

    /**
     * Permet de mettre en forme le produit à partir du JSON
     * @param barcode : le code barre du produit
     * @return Product : le produit mis en forme ou null si le code barre n'éxiste ni en BDD ni sur l'API
     */
    public Product getByBarcode(String barcode)
    {

        //Chercher le produit dans la BDD
        Product product = productRepository.findByBarcode(barcode);

        //Si le produit n'est pas dans la BDD
        if(product == null)
        {
            OFFProduct offProduct = offProductService.getByBarcode(barcode);

            //Si le produit existe sur l'API
            if(offProduct != null)
            {
                //Calcul de N et P
                int score = 0;
                int N_score = getNScore(offProduct);
                int P_score = getPScore(offProduct);
                score = N_score - P_score;

                //Récupération du nutrition score
                Nutrition_score ns = nutrition_scoreRepository.findByScore(score);

                //Insérer le produit dans la BDD
                product = productRepository.save(new Product(offProduct.getBarcode(),  offProduct.getGeneric_name(), score, ns.getClasse(), ns.getColor()));

            }
        }

        return product;


    }

    /**
     * Permet de calculer la composante N du score
     * @param offProduct : l'entity contenant les informations de l'API
     * @return int : la composante N
     */
    public int getNScore(OFFProduct offProduct)
    {
        String[] N = {"energy_100g", "saturated-fat_100g", "sugars_100g", "salt_100g"};

        int N_score = 0;

        //Pour tous les nutriments composants N
        for (String s: N)
        {
            //Essayer de récupérer les valeurs et les sommer
            try
            {
                N_score += ruleRepository.findByNameAndValue(s, offProduct.getNutrimentValue(s));
            }catch (Exception e) { }
        }

        return N_score;

    }

    /**
     * Permet de calculer la composante P du score
     * @param offProduct : l'entity contenant les informations de l'API
     * @return int : la composante P
     */
    public int getPScore(OFFProduct offProduct)
    {
        String[] P = {"fiber_100g", "proteins_100g"};

        int P_score = 0;

        //Pour tous les nutriments composants P
        for (String s: P)
        {
            //Essayer de récupérer les valeurs et les sommer
            try
            {
                P_score += ruleRepository.findByNameAndValue(s, offProduct.getNutrimentValue(s));;
            }
            catch (Exception e) { }
        }

        return P_score;
    }
}
