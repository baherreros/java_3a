package app.repository;

import app.entity.Rule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Permet de faire le lien entre la table rule et l'entity
 */
@Repository
public interface RuleRepository extends JpaRepository<Rule, Long> {

    /**
     * Permet de selectionner le score de "name" correspondant au score "value"
     * @param name : le nom du nutriment pour 100g
     * @param value : la valeur du nutriment à tester
     * @return Integer: les points du nutriment
     */
    @Query("SELECT MAX(r.points) FROM Rule r WHERE r.name = ?1 AND ?2 >= r.min_bound")
    Integer findByNameAndValue(String name, double value);
}

