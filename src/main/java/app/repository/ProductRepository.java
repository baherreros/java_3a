package app.repository;


import app.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Permet de faire le lien entre la table product et l'entity
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long>
{
    /**
     * Permet de récupérer un produit dans la BDD à partir de son code barre
     * @param barcode : le code barre du produit
     * @return Product : le produit ou null si il est inexistant
     */
    public Product findByBarcode(String barcode);
}
