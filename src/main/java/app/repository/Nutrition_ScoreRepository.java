package app.repository;

import app.entity.Nutrition_score;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Permet de faire le lien entre la table nutrition_score et l'entity
 */
@Repository
public interface Nutrition_ScoreRepository extends JpaRepository<Nutrition_score, Long> {

    /**
     * Permet de recuperer la classe et la couleur en fonction du score
     * @param score : le nutrition score du produit
     * @return Nutrition_score : l'entité contenant les informations concernant le score
     */
    @Query("SELECT ns FROM Nutrition_score ns WHERE ?1 >= ns.lower_bound AND ?1 <= ns.upper_bound")
    Nutrition_score findByScore(int score);
}

