package app.entity;

import javax.persistence.Entity;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Entity représentant les éléments de l'API
 */

public class OFFProduct
{
    private String barcode;
    private String generic_name;
    private Map<String, Double> nutriments;

    public void addNutriment(String name, Double value)
    {
        nutriments.put(name, value);
    }

    public Double getNutrimentValue(String name)
    {
        return nutriments.get(name);
    }

    public OFFProduct(String barcode, String generic_name) {
        this.barcode = barcode;
        this.generic_name = generic_name;
        this.nutriments = new HashMap<String, Double>();
    }

    public Map<String, Double> getNutriments() {
        return nutriments;
    }

    public void setNutriments(Map<String, Double> nutriments) {
        this.nutriments = nutriments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OFFProduct that = (OFFProduct) o;
        return barcode.equals(that.barcode) &&
                generic_name.equals(that.generic_name) &&
                nutriments.equals(that.nutriments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(barcode, generic_name, nutriments);
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getGeneric_name() {
        return generic_name;
    }

    public void setGeneric_name(String generic_name) {
        this.generic_name = generic_name;
    }
}
