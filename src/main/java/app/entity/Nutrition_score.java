package app.entity;

import javax.persistence.*;

/**
 * Entity représentant les éléments de la table nutrition_score
 */
@Entity
@Table(name = "nutrition_score")
public class Nutrition_score {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "classe")
    private String classe;

    @Column(name = "lower_bound")
    private int lower_bound;

    @Column(name = "upper_bound")
    private int upper_bound;

    @Column(name = "color")
    private String color;

    public Nutrition_score(){}

    public Nutrition_score(Long id, String classe, int lower_bound, int upper_bound, String color) {
        this.id = id;
        this.classe = classe;
        this.lower_bound = lower_bound;
        this.upper_bound = upper_bound;
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public int getLower_bound() {
        return lower_bound;
    }

    public void setLower_bound(int lower_bound) {
        this.lower_bound = lower_bound;
    }

    public int getUpper_bound() {
        return upper_bound;
    }

    public void setUpper_bound(int upper_bound) {
        this.upper_bound = upper_bound;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}

