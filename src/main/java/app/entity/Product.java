package app.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Entity représentant les éléments de la table product
 * La table contient les produits que l'on à déjà récupéré
 * Elle permet de ne pas avoir à recalculer les informations plusieurs fois pour un même produit
 */
@Entity
@Table(name = "product")
public class Product {

    @Column(name = "id" )
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Id
    private Long id;

    @Column(name = "barcode")
    private String barcode;

    @Column(name = "name")
    private String name;

    @Column(name = "nutrition_score")
    private int nutritionScore;

    @Column(name = "classe")
    private String classe;

    @Column(name = "color")
    private  String color;

    public Product(){}

    public Product(Long id, String barcode, String name, int nutritionScore, String classe, String color) {
        this.id = id;
        this.barcode = barcode;
        this.name = name;
        this.nutritionScore = nutritionScore;
        this.classe = classe;
        this.color = color;
    }

    public Product(String barcode, String name, int nutritionScore, String classe, String color) {
        this.barcode = barcode;
        this.name = name;
        this.nutritionScore = nutritionScore;
        this.classe = classe;
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return nutritionScore == product.nutritionScore &&
                Objects.equals(id, product.id) &&
                Objects.equals(barcode, product.barcode) &&
                Objects.equals(name, product.name) &&
                Objects.equals(classe, product.classe) &&
                Objects.equals(color, product.color);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNutritionScore() {
        return nutritionScore;
    }

    public void setNutritionScore(int nutritionScore) {
        this.nutritionScore = nutritionScore;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "ProductEntity{" +
                "id=" + id +
                ", barcode='" + barcode + '\'' +
                ", name='" + name + '\'' +
                ", nutritionScore=" + nutritionScore +
                ", classe='" + classe + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
