package app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Entity représentant les éléments de la table rule
 */
@Entity
@Table(name = "rule")
public class Rule {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "points")
    private int points;

    @Column(name = "min_bound")
    private double min_bound;

    @Column(name = "component")
    private char component;

    @Override
    public String toString() {
        return "Rule{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", points=" + points +
                ", min_bound=" + min_bound +
                ", component=" + component +
                '}';
    }

    public Rule(){}

    public Rule(Long id, String name, int points, double min_bound, char component) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.min_bound = min_bound;
        this.component = component;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public double getMin_bound() {
        return min_bound;
    }

    public void setMin_bound(double min_bound) {
        this.min_bound = min_bound;
    }

    public char getComponent() {
        return component;
    }

    public void setComponent(char component) {
        this.component = component;
    }
}

