package app.controller;

import app.service.ProductService;
import app.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *  API
 */
@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductService productService;

    /**
     * Méthode GET pour récupérer un produit à partir d'un code barre
     * @param barcode : le code barre du produit à trouver
     * @return Product : Le produit ou retourne 404 si le produit n'est pas trouvé
     */
    @GetMapping("/{barcode}")
    public Product getByBarcode(@PathVariable String barcode)
    {
        //Sanityze entrée en enlevant les caractères spéciaux
        String output = barcode.replaceAll("\"[{}()\\\\[\\\\].+*?^$\\\\\\\\|% ]\"", "");

        //Récupération du produit via le service
        Product p = productService.getByBarcode(output);

        //Si p est trouvé
        if(p != null)
            return p;
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find barcode");
    }
}
